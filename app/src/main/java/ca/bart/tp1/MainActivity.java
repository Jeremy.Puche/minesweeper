package ca.bart.tp1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity
{


    TextView Mines;


    public static final int COLS = 10;
    public static final int ROWS = 10;

    GridLayout myGrid;
    Button[] buttons = new Button[COLS * ROWS];

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myGrid = findViewById(R.id.GridLayout1);
        newGame();
        // Mine = (TextView) findViewById(R.id.Mines);

    }

    int[] grid = new int[COLS * ROWS];

    boolean hasMine(int x, int y)
    {
        return grid[(x + y * COLS) & 1] == 1;
    }

    boolean hasFlag(int x, int y){return  grid[(x + y * COLS) & 1] == 2;}

    int setMine(int x, int y)
    {
        return grid[x + y * COLS] |= 1;
    }

    void toogleFlag(int x, int y)
    {
        grid[x + y * COLS] ^= 4;
    }

    void newGame()
    {

        //final Button button = findViewById(R.id.button_id);
        for (int x = 0; x < COLS; ++x)
        {
            for (int y = 0; y < ROWS; y++)
            {
                if ((x + y * COLS) < 10)
                {
                    setMine(x, y);
                } else
                {
                    grid[x + y * COLS] = 0;
                }
                Log.d("myActivity", String.valueOf(myGrid.getChildAt(x + y * COLS)));
            }

        }
        Log.d("myActivity", String.valueOf(0 ^=4));
        shuffleArray(grid);
        refreshView();
    }

    void refreshView()
    {
        for (int x = 0; x < COLS; ++x)
        {
            for (int y = 0; y < ROWS; y++)
            {
//                if(hasFlag(x,y))
//                {
//
//                }
//			    else if(!isExposed(x,y))
//                {
//
//                };
            }

        }
    }

    void createButtonArray()
    {
        for (int i =0; i< myGrid.getChildCount();i++)
        {
            buttons[i] = (Button) myGrid.getChildAt(x + y * COLS);
        }
    }

    private static void shuffleArray(int[] array)
    {
        int index;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            if (index != i)
            {
                array[index] ^= array[i];
                array[i] ^= array[index];
                array[index] ^= array[i];
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        // outState.putString("savedName",hello.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        //  hello.setText (savedInstanceState.getString("savedName"));
    }
}
